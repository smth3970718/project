package storage

import (
	"context"
	"project/api/models"
)

type IStorage interface {
	Close()
	Branch() IBranchStorage
	BranchPoint() IBranchPointStorage
	MarketOrders() IMarketOrdersStorage
	Category() ICategoryStorage
	//Orders() IOrdersStorage
	Product() IProductStorage
}
type IBranchStorage interface {
	Create(context.Context, models.CreateBranch) (string, error)
	GetById(context.Context, models.PrimaryKey) (models.Branch, error)
	GetList(context.Context, models.GetlistRequest) (models.BranchResponse, error)
	Update(context.Context, models.UpdateBranch) (string, error)
	Delete(context.Context, models.PrimaryKey) error
	UpdateStatusBranch(context.Context, models.PrimaryKey) (string, error)
}
type IBranchPointStorage interface {
	Create(context.Context, models.CreateBranchPoint) (string, error)
	GetById(context.Context, models.PrimaryKey) (models.BranchPoint, error)
	GetList(context.Context, models.GetlistRequest) (models.BranchPointResponse, error)
	Update(context.Context, models.UpdateBranchPoint) (string, error)
	Delete(context.Context, models.PrimaryKey) error
	UpdateStatusBranchPoint(context.Context, models.UpdateStatus) (string, error)
}
type IMarketOrdersStorage interface {
	Create(context.Context, models.CreateMarketOrders) (string, error)
	GetById(context.Context, models.PrimaryKey) (models.MarketOrders, error)
	GetList(context.Context, models.GetlistRequest) (models.MarketOrdersResponse, error)
	Update(context.Context, models.UpdateMarketOrders) (string, error)
	Delete(context.Context, models.PrimaryKey) error
	SavingOrders(context.Context) error
	ListSavingOrders(context.Context, models.GetlistRequestOrder) (models.OrdersHistoryResponse, error)
	UpdateStatus(context.Context, models.UpdateStatus) (string, error)
	CheckStatus(context.Context,models.PrimaryKey)(string,error)
}
type ICategoryStorage interface {
	Create(context.Context, models.CreateCategory) (string, error)
	GetById(context.Context, models.PrimaryKey) (models.Category, error)
	GetList(context.Context, models.GetlistRequest) (models.CategoryResponse, error)
	Update(context.Context, models.UpdateCategory) (string, error)
	Delete(context.Context, models.PrimaryKey) error
}

//	type IOrdersStorage interface {
//		Create(context.Context, models.Orders) (string, error)
//		GetById(context.Context, models.PrimaryKey) (models.Orders, error)
//		GetList(context.Context, models.GetlistRequest) (models.OrdersResponse, error)
//		Delete(context.Context, models.PrimaryKey) error
//	}
type IProductStorage interface {
	Create(context.Context, models.CreateProduct) (string, error)
	GetById(context.Context, models.PrimaryKey) (models.Product, error)
	GetList(context.Context, models.GetlistRequest) (models.ProductResponse, error)
	Update(context.Context, models.UpdateProduct) (string, error)
	Delete(context.Context, models.PrimaryKey) error
}
