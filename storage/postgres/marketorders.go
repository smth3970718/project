package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"project/api/models"
	"project/pkg/logger"
	"project/storage"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type marketordersRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewMarketOrdersRepo(db *pgxpool.Pool, log logger.ILogger) storage.IMarketOrdersStorage {
	return &marketordersRepo{
		db:  db,
		log: log,
	}
}


func (m *marketordersRepo) Create(ctx context.Context, marketorders models.CreateMarketOrders) (string, error) {
	query := `INSERT INTO marketOrders(id, product_id, quantity, status) VALUES($1, $2, $3, $4)`
	id := uuid.New()
	if _, err := m.db.Exec(ctx, query, id, marketorders.ProductID, marketorders.Quantity, marketorders.Status); err != nil {
		m.log.Error("error while creating marketorders", logger.Error(err))
		return "", err
	}
	return id.String(), nil
}


func (m *marketordersRepo) GetById(ctx context.Context, id models.PrimaryKey) (models.MarketOrders, error) {
	market := models.MarketOrders{}
	var createdAt, updatedAt *time.Time
	var price sql.NullFloat64
	query := `select id,product_id,quantity,price,date,status, created_at, updated_at from marketOrders where id=$1 `
	if err := m.db.QueryRow(ctx, query, id.ID).Scan(&market.Id, &market.ProductID, &market.Quantity, &price, &market.Date, &market.Status, &createdAt, &updatedAt); err != nil {
		m.log.Error("error is while getting marketorders data", logger.Error(err))
		return market, err
	}
	if createdAt != nil {
		market.CreatedAt = *createdAt
	}
	if updatedAt != nil {
		market.UpdatedAt = *updatedAt
	}
	if price.Valid {
        market.Price = price.Float64
    }
	
	return market, nil

}

func (m *marketordersRepo) GetList(ctx context.Context, request models.GetlistRequest) (models.MarketOrdersResponse, error) {
	var (
		count             = 0
		marketorders      = []models.MarketOrders{}
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
	)

	countQuery = `select count(*) from marketOrders where deleted_at=0 and date=current_date `
	if search != "" {
		countQuery += fmt.Sprintf(` and status ='%s'`, search)
	}
	if err := m.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		m.log.Error("error is while scanning count", logger.Error(err))
		return models.MarketOrdersResponse{}, err
	}
	query = `select id,product_id,quantity,price,date,status,created_at,updated_at from marketOrders where deleted_at=0 and date=current_date  `
	if search != "" {
		query += fmt.Sprintf(` and status ='%s' `, search)
	}
	query += ` LIMIT $1 OFFSET $2 `
	rows, err := m.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		m.log.Error("error is while scanning", logger.Error(err))
		return models.MarketOrdersResponse{}, err
	}
	for rows.Next() {
		market := models.MarketOrders{}
		var createdAt, updatedAt *time.Time
		if err := rows.Scan(&market.Id, &market.ProductID, &market.Quantity, &market.Price, &market.Date, &market.Status, &createdAt, &updatedAt); err != nil {
			m.log.Error("error is while scanning", logger.Error(err))
			return models.MarketOrdersResponse{}, err
		}
		if createdAt != nil {
			market.CreatedAt = *createdAt
		}
		if updatedAt != nil {
			market.UpdatedAt = *updatedAt
		}
		marketorders = append(marketorders, market)

	}
	return models.MarketOrdersResponse{
		MarketOrders: marketorders,
		Count:        count,
	}, nil

}

func (m *marketordersRepo) Update(ctx context.Context, marketorders models.UpdateMarketOrders) (string, error) {
	query := `update marketOrders set quantity=$1, price=$2, product_id=$3, date=$4 where id=$5 `
	if rowsAffected, err := m.db.Exec(ctx, query, &marketorders.Quantity, &marketorders.Price, &marketorders.ProductID, &marketorders.Date, &marketorders.Id); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			m.log.Error("error is in rows affected", logger.Error(err))
			return "", err
		}
		m.log.Error("error is while updating marketorders", logger.Error(err))
		return "", err
	}
	return marketorders.Id, nil
}

func (m *marketordersRepo) Delete(ctx context.Context, id models.PrimaryKey) error {
	query := `update marketOrders  set deleted_at = extract(epoch from current_timestamp) where id = $1 `
	if rowsAffected, err := m.db.Exec(ctx, query, id.ID); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			m.log.Error("error is in rows affected", logger.Error(err))
			return err
		}
		m.log.Error("error is while updating marketorders", logger.Error(err))
		return err
	}
	return nil
}

func (m *marketordersRepo) SavingOrders(ctx context.Context) error {
	querytrigger := `CREATE OR REPLACE FUNCTION create_orders_history()
	RETURNS TRIGGER AS $$
	BEGIN
		-- Check if all market orders for the branch point are fully paid
		IF NOT EXISTS (
			SELECT 1 FROM marketOrders mo
			WHERE mo.branch_point_id = NEW.branch_point_id
			AND mo.status <> 'paid'
		) THEN
			-- Calculate total sum of market orders
			-- Calculate total sum of market orders
           DECLARE total_sum FLOAT;
           SELECT SUM(quantity * price) INTO total_sum FROM marketOrders WHERE branch_point_id = NEW.branch_point_id;
           
			-- Insert record into ordershistory with current date
			INSERT INTO ordershistory (id, marketorders_id, total_sum, created_at)
			VALUES (uuid_generate_v4(), NEW.branch_point_id, total_sum, CURRENT_DATE);
		END IF;
		RETURN NEW;
	END;
	$$ LANGUAGE plpgsql;	 `

	if _, err := m.db.Exec(ctx, querytrigger); err != nil {
		m.log.Error("error is while creating trigger", logger.Error(err))
		return err
	}
	return nil
}

func (m *marketordersRepo) ListSavingOrders(ctx context.Context, request models.GetlistRequestOrder) (models.OrdersHistoryResponse, error) {
	var (
		count             = 0
		savedorders       = []models.OrderHistory{}
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
	)

	countQuery = `select count(*) from ordersHistory `
	if search != "" {
		countQuery += fmt.Sprintf(` and market_orders_id ='%s'`, search)
	}
	if !request.FromDate.IsZero() && !request.ToDate.IsZero() {
		countQuery += fmt.Sprintf(` and created_at >='%s' and created_at <='%s'`, request.FromDate, request.ToDate)
	}

	if err := m.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		m.log.Error("error is while scanning count", logger.Error(err))
		return models.OrdersHistoryResponse{}, err
	}
	query = ` select id,market_orders_id,total_sum,created_at, updated_at from ordersHistory `
	if search != "" {
		query += fmt.Sprintf(` and market_orders_id ='%s' `, search)
	}
	if !request.FromDate.IsZero() && !request.ToDate.IsZero() {
		query += fmt.Sprintf(` and created_at >='%s' and created_at <='%s' `, request.FromDate, request.ToDate)
	}
	query += ` LIMIT $1 OFFSET $2 `
	rows, err := m.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		m.log.Error("error is while scanning", logger.Error(err))
		return models.OrdersHistoryResponse{}, err
	}
	for rows.Next() {
		order := models.OrderHistory{}
		var createdAt, updatedAt *time.Time
		if err := rows.Scan(&order.Id, &order.MarketOrdersID, &order.TotalSum, &createdAt, &updatedAt); err != nil {
			m.log.Error("error is while scanning", logger.Error(err))
			return models.OrdersHistoryResponse{}, err
		}
		if createdAt != nil {
			order.CreatedAt = *createdAt
		}
		if updatedAt != nil {
			order.UpdatedAt = *updatedAt
		}
		savedorders = append(savedorders, order)

	}
	return models.OrdersHistoryResponse{
		OrdersHistory: savedorders,
		Count:         count,
	}, nil
}
func (m *marketordersRepo) UpdateStatus(ctx context.Context, status models.UpdateStatus) (string, error) {
	query := ` update marketOrders set status=$1 where id=$2`
	if _, err := m.db.Exec(ctx, query, &status.Status, &status.Id); err != nil {
		//m.log.Error("error is while updating status", logger.Error(err))
		fmt.Println("error is while updating status")
	}
	fmt.Println("updated status")
	return status.Id, nil
}
func (m *marketordersRepo) CheckStatus(ctx context.Context, id models.PrimaryKey) (string, error) {
	query := `SELECT status FROM marketOrders WHERE product_id IN (SELECT id FROM product WHERE branch_point_id = $1)`
	rows, err := m.db.Query(ctx, query, id.ID)
	if err != nil {
		m.log.Error("error while querying market orders", logger.Error(err))
		return "", err
	}
	defer rows.Close()

	branchpointStatus := "fully_paid"
	/*
		>> initially fully_paid boladi va select qilingan rowlani statusi tekshirilib obshi one particular branchpoint's status is executed
	*/

	for rows.Next() {
		var status string
		if err := rows.Scan(&status); err != nil {
			m.log.Error("error while scanning market order status", logger.Error(err))
			return "", err
		}
		if status != "paid" {
			branchpointStatus = "not_fully_paid"
			break
		}
	}
	return branchpointStatus, nil
}
