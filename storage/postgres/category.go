package postgres

import (
	"context"
	"fmt"
	"project/api/models"
	"project/pkg/logger"
	"project/storage"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type categoryRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCategoryRepo(db *pgxpool.Pool, log logger.ILogger) storage.ICategoryStorage {
	return &categoryRepo{
		db:  db,
		log: log,
	}
}

func (c *categoryRepo) Create(ctx context.Context, category models.CreateCategory) (string, error) {
	query := `insert into category(id,name) values($1,$2)`
	id := uuid.New()
	if rowsAffected, err := c.db.Exec(ctx, query, id, category.Name); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			c.log.Error("error is rows affected", logger.Error(err))
			return "", err
		}
		c.log.Error("error is while creating category", logger.Error(err))
		return "", err
	}
	return id.String(), nil
}

func (c *categoryRepo) GetById(ctx context.Context, category models.PrimaryKey) (models.Category, error) {
	query := `select id,name,created_at,updated_at from category where id=$1 and deleted_at=0`
	catg := models.Category{}
	var createdat, updatedAt *time.Time
	if err := c.db.QueryRow(ctx, query, category.ID).Scan(&catg.Id, &catg.Name, &createdat, &updatedAt); err != nil {
		c.log.Error("error is while getting category data", logger.Error(err))
		return catg, err
	}
	if createdat != nil {
		catg.CreatedAt = *createdat
	}
	if updatedAt != nil {
		catg.UpdatedAt = *updatedAt
	}
	return catg, nil
}

func (c *categoryRepo) GetList(ctx context.Context, request models.GetlistRequest) (models.CategoryResponse, error) {
	var (
		count             = 0
		categories        = []models.Category{}
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
	)
	countQuery = `select count(*) from category where deleted_at=0 `
	if search != "" {
		countQuery += fmt.Sprintf(` and name ilike '%s'`, search)
	}
	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error is while scanning count", logger.Error(err))
		return models.CategoryResponse{}, err
	}
	query = `select id, name, created_at,updated_at from category where deleted_at=0 `
	if search != "" {
		query += fmt.Sprintf(` and name ilike '%s' `, search)
	}
	query += ` LIMIT $1 OFFSET $2 `
	rows, err := c.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		c.log.Error("error is while selecting all", logger.Error(err))
		return models.CategoryResponse{}, err
	}
	for rows.Next() {
		cats := models.Category{}
		var createdat, updatedAt *time.Time
		if err := rows.Scan(&cats.Id, &cats.Name, &createdat, &updatedAt); err != nil {
			c.log.Error("error is while scanning", logger.Error(err))
			return models.CategoryResponse{}, err
		}
		if createdat != nil {
			cats.CreatedAt = *createdat
		}
		if updatedAt != nil {
			cats.UpdatedAt = *updatedAt
		}
		categories = append(categories, cats)
	}
	return models.CategoryResponse{
		Categories: categories,
		Count:      count,
	}, nil
}

func (c *categoryRepo) Update(ctx context.Context, category models.UpdateCategory) (string, error) {
	query := ` update category set name=$1, updated_at=now() where id=$2 `
	if rowsAffected, err := c.db.Exec(ctx, query, &category.Name, &category.Id); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			c.log.Error("error is in rows affected", logger.Error(err))
			return "", err
		}
		c.log.Error("error is while updating category", logger.Error(err))
		return "", err
	}
	return category.Id, nil
}

func (c *categoryRepo) Delete(ctx context.Context, id models.PrimaryKey) error {
	query := `update category  set deleted_at = extract(epoch from current_timestamp) where id = $1 `
	if rowsAffected, err := c.db.Exec(ctx, query, id.ID); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			c.log.Error("error is in rows affected", logger.Error(err))
			return err
		}
		c.log.Error("error is while updating category", logger.Error(err))
		return err
	}
	return nil
}
