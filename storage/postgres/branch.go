package postgres

import (
	"context"
	"fmt"
	"project/api/models"
	"project/pkg/logger"
	"project/storage"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type branchRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewBranchRepo(db *pgxpool.Pool, log logger.ILogger) storage.IBranchStorage {
	return &branchRepo{
		db:  db,
		log: log,
	}
}

func (b *branchRepo) Create(ctx context.Context, branch models.CreateBranch) (string, error) {
	idbranch := uuid.New()
	query := `insert into branch(id,name) values($1,$2)`
	if rowsAffected, err := b.db.Exec(ctx, query, idbranch, branch.Name); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			b.log.Error("error is rows affected", logger.Error(err))
			return "", err
		}
		b.log.Error("error is while inserting branch data", logger.Error(err))
		return "", err
	}
	return idbranch.String(), nil
}
func (b *branchRepo) GetById(ctx context.Context, branch models.PrimaryKey) (models.Branch, error) {
	bbranch := models.Branch{}
	var createdAt, updatedAt *time.Time // Use time.Time instead of sql.NullString
	query := `SELECT id, name, created_at, updated_at FROM branch WHERE id=$1 AND deleted_at=0`
	if err := b.db.QueryRow(ctx, query, branch.ID).Scan(&bbranch.Id, &bbranch.Name, &createdAt, &updatedAt); err != nil {
		b.log.Error("error while getting branch data", logger.Error(err))
		return bbranch, err
	}

	// Assign timestamps directly to struct fields
	if createdAt != nil {
		bbranch.CreatedAt = *createdAt
	}
	if updatedAt != nil {
		bbranch.UpdatedAt = *updatedAt
	}

	return bbranch, nil
}

func (b *branchRepo) GetList(ctx context.Context, request models.GetlistRequest) (models.BranchResponse, error) {
	var (
		count             = 0
		branches          = []models.Branch{}
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
	)

	countQuery = `select count(1) from branch where deleted_at=0 and date=current_date `
	if search != "" {
		countQuery += fmt.Sprintf(` and name ilike '%s'`, search)
	}
	if err := b.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		b.log.Error("error is while scanning count", logger.Error(err))
		return models.BranchResponse{}, err
	}
	query = `select id, name,created_at,updated_at from branch where deleted_at=0 and date=current_date `
	if search != "" {
		query += fmt.Sprintf(` and name ilike '%s' `, search)
	}

	query += ` LIMIT $1 OFFSET $2 `
	rows, err := b.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		b.log.Error("error is while selecting all", logger.Error(err))
		return models.BranchResponse{}, err
	}

	for rows.Next() {

		bbranch := models.Branch{}
		var createdAt, updatedAt *time.Time
		if err := rows.Scan(&bbranch.Id, &bbranch.Name, &createdAt, &updatedAt); err != nil {
			b.log.Error("error is while scanning", logger.Error(err))
			return models.BranchResponse{}, err
		}
		if createdAt != nil {
			bbranch.CreatedAt = *createdAt
		}
		if updatedAt != nil {
			bbranch.UpdatedAt = *updatedAt
		}
		branches = append(branches, bbranch)
	}

	return models.BranchResponse{
		Branches: branches,
		Count:    count,
	}, nil

}
func (b *branchRepo) Update(ctx context.Context, branch models.UpdateBranch) (string, error) {

	query := ` update branch set name=$1, updated_at=now() where id=$2 `

	if rowsAffected, err := b.db.Exec(ctx, query, &branch.Name, &branch.Id); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			b.log.Error("error is in rows affected", logger.Error(err))
			return "", err
		}
		b.log.Error("error is while updating branch", logger.Error(err))
		return "", err
	}
	return branch.Id, nil
}
func (b *branchRepo) Delete(ctx context.Context, id models.PrimaryKey) error {
	query := `update branch  set deleted_at = extract(epoch from current_timestamp) where id = $1 `
	if rowsAffected, err := b.db.Exec(ctx, query, id.ID); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			b.log.Error("error is in rows affected", logger.Error(err))
			return err
		}
		b.log.Error("error is while deleting branch", logger.Error(err))
		return err
	}
	return nil

}

func (b *branchRepo) UpdateStatusBranch(ctx context.Context, request models.PrimaryKey) (string, error) {
	query1 := `select status from branchpoint where id=$1`
	rows, err := b.db.Query(ctx, query1, request.ID)
	if err != nil {
		b.log.Error("error is while checking status", logger.Error(err))
		return "", err
	}
	defer rows.Close()

	var branchStatus string
	allPaid := true
	for rows.Next() {
		var status string
		if err := rows.Scan(&status); err != nil {
			return "", err
		}
		if status != "paid" {
			allPaid = false
			break
		}
	}
	if allPaid {
		branchStatus = "fully_paid"
	} else {
		branchStatus = "not_paid"
	}
	_, err = b.db.Exec(ctx, `UPDATE branch SET status = $1 WHERE id = $2`, branchStatus, request.ID)
	if err != nil {
		return "", err
	}

	return request.ID, nil
}
