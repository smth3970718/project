package postgres

import (
	"context"
	"fmt"
	"project/api/models"
	"project/pkg/helper"
	"project/pkg/logger"
	"project/storage"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type productRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewProductRepo(db *pgxpool.Pool, log logger.ILogger) storage.IProductStorage {
	return &productRepo{
		db:  db,
		log: log,
	}
}

func (p *productRepo) Create(ctx context.Context, product models.CreateProduct) (string, error) {
	query := `insert into product(id, name, price, category_id,branch_point_id) values($1,$2,$3,$4,$5)`
	id := uuid.New()
	_, err := p.db.Exec(ctx, query, id, product.Name, product.Price, product.CategoryID, product.BranchPointID)
	if err != nil {
		p.log.Error("ERROR in repo layer while creating product", logger.Error(err))
		return "", err
	}
	return id.String(), nil

}

func (p *productRepo) GetById(ctx context.Context, id models.PrimaryKey) (models.Product, error) {
	query := `select id, name, price, category_id, branch_point_id, created_at, updated_at from product where id = $1`
	var product models.Product
	var createdAt, updatedAt *time.Time
	err := p.db.QueryRow(ctx, query, id.ID).Scan(&product.Id, &product.Name, &product.Price, &product.CategoryID, &product.BranchPointID, &createdAt, &updatedAt)
	if err != nil {
		p.log.Error("ERROR in repo layer while getting by id product", logger.Error(err))
		return models.Product{}, err
	}
	if createdAt != nil {
		product.CreatedAt = *createdAt
	}
	if updatedAt != nil {
		product.UpdatedAt = *updatedAt
	}

	return product, nil
}

func (p *productRepo) GetList(ctx context.Context, request models.GetlistRequest) (models.ProductResponse, error) {
	var (
		count             = 0
		products          = []models.Product{}
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
	)
	countQuery = `select count(*) from product where deleted_at=0 `
	if search != "" {
		countQuery += fmt.Sprintf(` and name ilike '%s'`, search)
	}
	if err := p.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		p.log.Error("error is while scanning count", logger.Error(err))
		return models.ProductResponse{}, err
	}
	query = `select id, name, price, category_id, branch_point_id, created_at, updated_at from product where deleted_at=0 `
	if search != "" {
		query += fmt.Sprintf(` and name ilike '%s' `, search)
	}
	query += ` LIMIT $1 OFFSET $2 `
	rows, err := p.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		p.log.Error("error is while selecting all", logger.Error(err))
		return models.ProductResponse{}, err
	}
	for rows.Next() {
		product := models.Product{}
		var createdAt, updatedAt *time.Time
		if err := rows.Scan(&product.Id, &product.Name, &product.Price, &product.CategoryID, &product.BranchPointID, &createdAt, &updatedAt); err != nil {
			p.log.Error("error is while scanning", logger.Error(err))
			return models.ProductResponse{}, err
		}
		if createdAt != nil {
			product.CreatedAt = *createdAt
		}
		if updatedAt != nil {
			product.UpdatedAt = *updatedAt
		}
		products = append(products, product)
	}
	return models.ProductResponse{
		Products: products,
		Count:    count,
	}, nil
}

func (p *productRepo) Update(ctx context.Context, product models.UpdateProduct) (string, error) {
	var (
		request = models.Product{}
		params  = make(map[string]interface{})
		query   = ` UPDATE product SET `
		filter  = ""
	)
	if product.Name != "" {
		params["name"] = product.Name
		query += "name = @name, "
	}
	if product.Price != 0.0 {
		params["price"] = product.Price
		query += "price = @price, "
	}
	if product.CategoryID != "" {
		params["category_id"] = product.CategoryID
		query += "category_id = @category_id, "
	}
	if product.BranchPointID != "" {
		params["branch_point_id"] = product.BranchPointID
		query += "branch_point_id = @branch_point_id, "
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, name, price, category_id, branch_point_id  `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := p.db.QueryRow(ctx, fullQuery, args...).Scan(&request.Id, &request.Name, &request.Price, &request.CategoryID, &request.BranchPointID); err != nil {
		return "", err
	}

	return request.Id, nil
}

func (p *productRepo) Delete(ctx context.Context, id models.PrimaryKey) error {
	query := `update product set deleted_at =extract(epoch from current_timestamp) where id = $1`
	_, err := p.db.Exec(ctx, query, id.ID)
	if err != nil {
		p.log.Error("ERROR in repo layer while deleting product", logger.Error(err))
		return err
	}
	return nil
}
