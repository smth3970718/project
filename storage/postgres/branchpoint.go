package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"project/api/models"
	"project/pkg/logger"
	"project/storage"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type branchPointRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewBranchPOintRepo(db *pgxpool.Pool, log logger.ILogger) storage.IBranchPointStorage {
	return &branchPointRepo{
		db:  db,
		log: log,
	}
}
func (b *branchPointRepo) Create(ctx context.Context, branchpoint models.CreateBranchPoint) (string, error) {
	query := `insert into branchpoint(id, name, address, branch_id) values($1,$2,$3,$4)`
	uuid := uuid.New()
	if rowsAffected, err := b.db.Exec(ctx, query, uuid, branchpoint.Name, branchpoint.Address, branchpoint.BranchId); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			b.log.Error("error is rows affected", logger.Error(err))
			return "", err
		}
	}
	return uuid.String(), nil

}
func (b *branchPointRepo) GetById(ctx context.Context, branchpoint models.PrimaryKey) (models.BranchPoint, error) {
	branchp := models.BranchPoint{}
	var createdat, updatedAt *time.Time
	var address *sql.NullString
	query := `select id,name,address,branch_id,created_at,updated_at from branchpoint where id=$1 and deleted_at=0`
	if err := b.db.QueryRow(ctx, query, branchpoint.ID).Scan(&branchp.ID, &branchp.Name, &address, &branchp.BranchId, &createdat, &updatedAt); err != nil {
		b.log.Error("error is while getting branchpoint data", logger.Error(err))
		return branchp, err
	}
	if address.Valid {
		branchp.Address = address.String
	}
	if createdat!= nil {
		branchp.CreatedAt = *createdat
	}
	if updatedAt!= nil {
		branchp.UpdatedAt = *updatedAt
	}
	return branchp, nil
}

func (b *branchPointRepo) GetList(ctx context.Context, request models.GetlistRequest) (models.BranchPointResponse, error) {
	var (
		count             = 0
		branchpoints      = []models.BranchPoint{}
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
	)
	countQuery = `select count(*) from branchpoint where deleted_at=0 and date=current_date `
	if search != "" {
		countQuery += fmt.Sprintf(` and status ='%s'`, search)
	}
	if err := b.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		b.log.Error("error is while scanning count", logger.Error(err))
		return models.BranchPointResponse{}, err
	}
	query = `select id,name,address,branch_id,created_at,updated_at from branchpoint where deleted_at=0 and date=current_date `
	if search != "" {
		query += fmt.Sprintf(` and status ='%s' `, search)
	}
	query += ` LIMIT $1 OFFSET $2 `
	rows, err := b.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		b.log.Error("error is while scanning", logger.Error(err))
		return models.BranchPointResponse{}, err
	}

	for rows.Next() {
		bpoint := models.BranchPoint{}
		var createdAt, updatedAt *time.Time
		var address *sql.NullString
		if err := rows.Scan(&bpoint.ID, &bpoint.Name, &address, &bpoint.BranchId, &createdAt, &updatedAt); err != nil {
			b.log.Error("error is while scanning", logger.Error(err))
			return models.BranchPointResponse{}, err
		}
		if address.Valid {
			bpoint.Address = address.String
		}
		if createdAt != nil {
			bpoint.CreatedAt = *createdAt
		}
		if updatedAt != nil {
			bpoint.UpdatedAt = *updatedAt
		}
		branchpoints = append(branchpoints, bpoint)
	}

	return models.BranchPointResponse{
		BranchPoints: branchpoints,
		Count:        count,
	}, nil
}

func (b *branchPointRepo) Update(ctx context.Context, branchpoint models.UpdateBranchPoint) (string, error) {
	query := ` update branchpoint set name=$1,address=$2,branch_id=$3,status=$4,updated_at=now() where id=$5 `
	if rowsAffected, err := b.db.Exec(ctx, query, &branchpoint.Name, &branchpoint.Address, &branchpoint.BranchId, &branchpoint.Status, &branchpoint.ID); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			b.log.Error("error is in rows affected", logger.Error(err))
			return "", err
		}
	}
	return branchpoint.ID, nil
}

func (b *branchPointRepo) Delete(ctx context.Context, id models.PrimaryKey) error {
	query := `update branchpoint set deleted_at=extract(epoch from current_timestamp) where id=$1 `
	if rowsAffected, err := b.db.Exec(ctx, query, id.ID); err != nil {
		if r := rowsAffected.RowsAffected(); r == 0 {
			b.log.Error("error is in rows affected", logger.Error(err))
			return err
		}
		b.log.Error("error is while deleting branchpoint", logger.Error(err))
		return err
	}
	return nil
}

func (b *branchPointRepo) UpdateStatusBranchPoint(ctx context.Context, request models.UpdateStatus) (string, error) {

	query := `update branchpoint set status=$1 where id=$2`
	if _, err := b.db.Exec(ctx, query, &request.Status, &request.Id); err != nil {
		b.log.Error("error is while updating status", logger.Error(err))
		return "", err
	}
	return request.Id, nil
}
