package service

import (
	"context"
	"project/api/models"
	"project/pkg/logger"
	"project/storage"
)

type branchPointService struct {
	storage storage.IStorage
	log     logger.ILogger
}

func NewBranchPointService(storage storage.IStorage, log logger.ILogger) branchPointService {
	return branchPointService{
		storage: storage,
		log:     log,
	}
}

func (b branchPointService) Create(ctx context.Context, branchpoint models.CreateBranchPoint) (models.BranchPoint, error) {
	id, err := b.storage.BranchPoint().Create(ctx, branchpoint)
	if err != nil {
		b.log.Error("ERROR in service layer while creating branchpoint", logger.Error(err))
		return models.BranchPoint{}, err
	}
	bp, err := b.storage.BranchPoint().GetById(ctx, models.PrimaryKey{ID: id})
	if err != nil {
		b.log.Error("ERROR in service layer while getting by id branchpoint", logger.Error(err))
		return models.BranchPoint{}, err
	}

	return bp, nil
}

func (b branchPointService) GetById(ctx context.Context, branchpoint models.PrimaryKey) (models.BranchPoint, error) {
	branches, err := b.storage.BranchPoint().GetById(ctx, branchpoint)
	if err != nil {
		b.log.Error("ERROR in service layer while getting by id branchpoint", logger.Error(err))
		return models.BranchPoint{}, err
	}
	return branches, nil
}

func (b branchPointService) GetList(ctx context.Context, request models.GetlistRequest) (models.BranchPointResponse, error) {
	branches, err := b.storage.BranchPoint().GetList(ctx, request)
	if err != nil {
		b.log.Error("ERROR in service layer while getting list of branchpoints", logger.Error(err))
		return models.BranchPointResponse{}, err
	}
	return branches, nil
}
func (b branchPointService) Update(ctx context.Context, branchpoint models.UpdateBranchPoint) (string, error) {
	return b.storage.BranchPoint().Update(ctx, branchpoint)
}

func (b branchPointService) Delete(ctx context.Context, id models.PrimaryKey) error {
	return b.storage.BranchPoint().Delete(ctx, id)
}

func (b branchPointService) UpdateStatusBranchPoint(ctx context.Context, request models.UpdateStatus) (string, error) {
	statusbranchpint, err := b.storage.MarketOrders().CheckStatus(ctx, models.PrimaryKey{ID: request.Id})
	if err != nil {
		b.log.Error("ERROR in service layer while checking status", logger.Error(err))
		return "", err
	}
	id, err := b.storage.BranchPoint().UpdateStatusBranchPoint(ctx, models.UpdateStatus{Id: request.Id, Status: statusbranchpint})
	if err != nil {
		b.log.Error("error is while updating status", logger.Error(err))
		return "", err
	}
	return id, nil
}
