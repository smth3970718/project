package service

import (
	"context"
	"project/api/models"
	"project/pkg/logger"
	"project/storage"
)

type branchService struct {
	storage storage.IStorage
	log     logger.ILogger
}

func NewBranchService(storage storage.IStorage, log logger.ILogger) branchService {
	return branchService{
		storage: storage,
		log:     log,
	}
}
func (b branchService) Create(ctx context.Context, branch models.CreateBranch) (models.Branch, error) {
	id, err := b.storage.Branch().Create(ctx, branch)
	if err != nil {
		b.log.Error("ERROR in service layer while creating branch", logger.Error(err))
		return models.Branch{}, err
	}

	branches, err := b.storage.Branch().GetById(ctx, models.PrimaryKey{ID: id})
	if err != nil {
		b.log.Error("ERROR in service layer while getting by id branch", logger.Error(err))
		return models.Branch{}, err
	}
	return branches, nil
}

func (b branchService) GetById(ctx context.Context, branch models.PrimaryKey) (models.Branch, error) {
	branches, err := b.storage.Branch().GetById(ctx, branch)
	if err != nil {
		b.log.Error("ERROR in service layer while getting by id branch", logger.Error(err))
		return models.Branch{}, err
	}
	return branches, nil

}
func (b branchService) GetList(ctx context.Context, branch models.GetlistRequest) (models.BranchResponse, error) {
	branches, err := b.storage.Branch().GetList(ctx, branch)
	if err != nil {
		b.log.Error("ERROR in service layer while getting list of branchs", logger.Error(err))
		return models.BranchResponse{}, err
	}
	return branches, nil

}

func (b branchService) Update(ctx context.Context, branch models.UpdateBranch) (string, error) {
	id, err := b.storage.Branch().Update(ctx, branch)
	if err != nil {
		b.log.Error("ERROR in service layer while updating branch", logger.Error(err))
		return "", err
	}
	return id, nil
}

func (b branchService) Delete(ctx context.Context, id models.PrimaryKey) error {
	err := b.storage.Branch().Delete(ctx, id)
	if err != nil {
		b.log.Error("ERROR in service layer while deleting branch", logger.Error(err))
		return err
	}
	return nil
}

func (b branchService) UpdateStatusBranch(ctx context.Context, request models.PrimaryKey) (string, error) {
	id, err := b.storage.Branch().UpdateStatusBranch(ctx, models.PrimaryKey{ID: request.ID})
	if err != nil {
		b.log.Error("ERROR in service layer while updating status branch", logger.Error(err))
		return "", err
	}
	return id, nil
}
