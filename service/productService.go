package service

import (
	"context"
	"project/api/models"
	"project/pkg/logger"
	"project/storage"
)

type productService struct {
	storage storage.IStorage
	log     logger.ILogger
}

func NewProductService(storage storage.IStorage, log logger.ILogger) productService {
	return productService{
		storage: storage,
		log:     log,
	}
}

func (p productService) Create(ctx context.Context, product models.CreateProduct) (models.Product, error) {

	id, err := p.storage.Product().Create(ctx, product)
	if err != nil {
		p.log.Error("ERROR in service layer while creating product", logger.Error(err))
		return models.Product{}, err
	}
	productt, err := p.storage.Product().GetById(ctx, models.PrimaryKey{ID: id})
	if err != nil {
		p.log.Error("ERROR in service layer while getting by id product", logger.Error(err))
		return models.Product{}, err
	}
	return productt, nil
}

func (p productService) GetById(ctx context.Context, product models.PrimaryKey) (models.Product, error) {
	productt, err := p.storage.Product().GetById(ctx, product)
	if err != nil {
		p.log.Error("ERROR in service layer while getting by id product", logger.Error(err))
		return models.Product{}, err
	}
	return productt, nil

}
func (p productService) GetList(ctx context.Context, product models.GetlistRequest) (models.ProductResponse, error) {
	products, err := p.storage.Product().GetList(ctx, product)
	if err != nil {
		p.log.Error("ERROR in service layer while getting list of products", logger.Error(err))
		return models.ProductResponse{}, err
	}
	return products, nil

}

func (p productService) Update(ctx context.Context, product models.UpdateProduct) (string, error) {
	id, err := p.storage.Product().Update(ctx, product)
	if err != nil {
		p.log.Error("ERROR in service layer while updating product", logger.Error(err))
		return "", err
	}
	return id, nil
}

func (p productService) Delete(ctx context.Context, id models.PrimaryKey) error {
	err := p.storage.Product().Delete(ctx, id)
	if err != nil {
		p.log.Error("ERROR in service layer while deleting product", logger.Error(err))
		return err
	}
	return nil
}
