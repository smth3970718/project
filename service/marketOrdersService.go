package service

import (
	"context"
	"fmt"
	"math"
	"project/api/models"
	"project/pkg/logger"
	"project/storage"
)

type marketOrdersService struct {
	storage storage.IStorage
	log     logger.ILogger
}

func NewMarketOrdersService(storage storage.IStorage, logger logger.ILogger) marketOrdersService {
	return marketOrdersService{
		storage: storage,
		log:     logger,
	}
}

func (m marketOrdersService) Create(ctx context.Context, marketorders models.CreateMarketOrders) (models.MarketOrders, error) {
	calculatedProductSum, err := m.storage.Product().GetById(ctx, models.PrimaryKey{ID: marketorders.ProductID})
	if err != nil {
		m.log.Error("ERROR in service layer while getting by id product", logger.Error(err))
		return models.MarketOrders{}, err
	}

	totalPrice := calculatedProductSum.Price * float64(marketorders.Quantity)
	roundedTotalPrice := math.Round(totalPrice*100) / 100
	marketorders.Price = roundedTotalPrice
	fmt.Println(marketorders.Price)
	id, err := m.storage.MarketOrders().Create(ctx, marketorders)
	if err != nil {
		m.log.Error("ERROR in service layer while creating marketorders", logger.Error(err))
		return models.MarketOrders{}, err
	}

	marketorder, err := m.storage.MarketOrders().GetById(ctx, models.PrimaryKey{ID: id})
	if err != nil {
		m.log.Error("ERROR in service layer while getting by id marketorders", logger.Error(err))
		return models.MarketOrders{}, err
	}

	return marketorder, nil
}

func (m marketOrdersService) GetById(ctx context.Context, marketorders models.PrimaryKey) (models.MarketOrders, error) {
	marketorder, err := m.storage.MarketOrders().GetById(ctx, marketorders)
	if err != nil {
		m.log.Error("ERROR in service layer while getting by id marketorders", logger.Error(err))
		return models.MarketOrders{}, err
	}
	return marketorder, nil
}

func (m marketOrdersService) GetList(ctx context.Context, request models.GetlistRequest) (models.MarketOrdersResponse, error) {
	marketorders, err := m.storage.MarketOrders().GetList(ctx, request)
	if err != nil {
		m.log.Error("ERROR in service layer while getting list of marketorders", logger.Error(err))
		return models.MarketOrdersResponse{}, err
	}
	return marketorders, nil
}

func (m marketOrdersService) Update(ctx context.Context, marketorders models.UpdateMarketOrders) (string, error) {
	return m.storage.MarketOrders().Update(ctx, marketorders)
}

func (m marketOrdersService) Delete(ctx context.Context, id models.PrimaryKey) error {
	return m.storage.MarketOrders().Delete(ctx, id)
}

func (m marketOrdersService) ListSavingOrders(ctx context.Context, id models.GetlistRequestOrder) (models.OrdersHistoryResponse, error) {
	orders, err := m.storage.MarketOrders().ListSavingOrders(ctx, id)
	if err != nil {
		m.log.Error("ERROR in service layer while getting list of marketorders", logger.Error(err))
		return models.OrdersHistoryResponse{}, err
	}
	return orders, nil
}

func (m marketOrdersService) SavingOrders(ctx context.Context) error {
	return m.storage.MarketOrders().SavingOrders(ctx)
}

func (m marketOrdersService) UpdateStatus(ctx context.Context, status models.UpdateStatus) (string, error) {
	u, err := m.storage.MarketOrders().UpdateStatus(ctx, status)
	if err != nil {
		m.log.Error("ERROR in service layer while updating status", logger.Error(err))
		return "", err
	}
	// err = m.storage.MarketOrders().SavingOrders(ctx)
	// if err != nil {
	// 	m.log.Error("ERROR", logger.Error(err))
	// 	return "", err
	// }
	return u, nil
}

func (m marketOrdersService) CheckStatus(ctx context.Context, id models.PrimaryKey) (string, error) {
	status, err := m.storage.MarketOrders().CheckStatus(ctx, id)
	if err != nil {
		m.log.Error("ERROR", logger.Error(err))
		return "", err
	}
	return status, nil
}
