package service

import (
	"context"
	"project/api/models"
	"project/pkg/logger"
	"project/storage"
)

type categoryService struct {
	storage storage.IStorage
	log     logger.ILogger
}

func NewCategoryService(storage storage.IStorage, log logger.ILogger) categoryService {
	return categoryService{
		storage: storage,
		log:     log,
	}
}

func (c categoryService) Create(ctx context.Context, category models.CreateCategory) (models.Category, error) {
	id, err := c.storage.Category().Create(ctx, category)
	if err != nil {
		c.log.Error("ERROR in service layer while creating category", logger.Error(err))
		return models.Category{}, err
	}
	cat, err := c.storage.Category().GetById(ctx, models.PrimaryKey{ID: id})
	if err != nil {
		c.log.Error("ERROR in service layer while getting by id category", logger.Error(err))
		return models.Category{}, err
	}
	return cat, nil
}

func (c categoryService) GetById(ctx context.Context, category models.PrimaryKey) (models.Category, error) {
	categories, err := c.storage.Category().GetById(ctx, category)
	if err != nil {
		c.log.Error("ERROR in service layer while getting by id category", logger.Error(err))
		return models.Category{}, err
	}
	return categories, nil
}

func (c categoryService) GetList(ctx context.Context, request models.GetlistRequest) (models.CategoryResponse, error) {
	categories, err := c.storage.Category().GetList(ctx, request)
	if err != nil {
		c.log.Error("ERROR in service layer while getting list of categories", logger.Error(err))
		return models.CategoryResponse{}, err
	}
	return categories, nil
}

func (c categoryService) Update(ctx context.Context, category models.UpdateCategory) (string, error) {
	return c.storage.Category().Update(ctx, category)
}

func (c categoryService) Delete(ctx context.Context, id models.PrimaryKey) error {
	return c.storage.Category().Delete(ctx, id)
}
