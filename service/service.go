package service

import (
	"project/pkg/logger"
	"project/storage"
)

type IServiceManager interface {
	//Users() userService
	Category() categoryService
	Branch() branchService
	BranchPoint() branchPointService
	Product() productService
	//Orders() ordersService
	MarketOrders() marketOrdersService
}

type Servicee struct {
	//userService        userService
	categoryServie      categoryService
	branchService       branchService
	productService      productService
	branchPointService  branchPointService
	marketOrdersService marketOrdersService
	//ordersService      ordersService
}

func New(storage storage.IStorage, log logger.ILogger) Servicee {
	services := Servicee{}
	services.categoryServie = NewCategoryService(storage, log)
	services.branchService = NewBranchService(storage, log)
	services.productService = NewProductService(storage, log)
	services.branchPointService = NewBranchPointService(storage, log)
	services.marketOrdersService = NewMarketOrdersService(storage, log)
	//services.ordersService = NewOrdersService(storage, log)
	return services

}


func (s Servicee) Category() categoryService {
    return s.categoryServie
}
func (s Servicee) Branch() branchService {
    return s.branchService
}
func (s Servicee) BranchPoint() branchPointService {
    return s.branchPointService
}
func (s Servicee) Product() productService {
    return s.productService
}
// func (s Servicee) Orders() ordersService {
	// return s.ordersService
// }
func (s Servicee) MarketOrders() marketOrdersService {
    return s.marketOrdersService
}

// func (s Servicee) Users() userService {
	// return s.userService
// }

