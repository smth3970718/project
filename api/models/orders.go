package models

import "time"

type OrderHistory struct {
	Id             string `json:"id"`
	MarketOrdersID string `json:"market_orders_id"`
	//Date           string  `json:"date"`
	TotalSum  float64 `json:"total_sum"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
}
type OrdersHistoryResponse struct {
	OrdersHistory []OrderHistory `json:"orders_history"`
	Count         int            `json:"count"`
}
type GetlistRequestOrder struct {
	Limit    int       `json:"limit"`
	Page     int       `json:"page"`
	Search   string    `json:"search"`
	FromDate time.Time `json:"from_date"`
	ToDate   time.Time `json:"to_date"`
}
