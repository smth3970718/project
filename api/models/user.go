package models

type User struct {
	ID       string `json:"id"`
	FullName string `json:"full_name"`
	Phone    string `json:"phone"`
}
type CreateUser struct {
	FullName string `json:"full_name"`
	Phone    string `json:"phone"`
}
type UpdateUser struct {
	ID       string `json:"id"`
	FullName string `json:"full_name"`
	Phone    string `json:"phone"`
}

