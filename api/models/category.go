package models

import "time"

type Category struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
type CreateCategory struct {
	Name string `json:"name"`
}
type UpdateCategory struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}
type CategoryResponse struct {
	Categories []Category `json:"categories"`
	Count      int        `json:"count"`
}
