package models

import "time"

type BranchPoint struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Address   string `json:"address"`
	Status    string `json:"status"`
	BranchId  string `json:"branch_id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
type CreateBranchPoint struct {
	Name     string `json:"name"`
	Address  string `json:"address"`
	//Status   string `json:"status"`
	BranchId string `json:"branch_id"`
}
type UpdateBranchPoint struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Address  string `json:"address"`
	Status   string `json:"status"`
	BranchId string `json:"branch_id"`
}
type BranchPointResponse struct {
	BranchPoints []BranchPoint `json:"branchPoints"`
	Count        int           `json:"count"`
}
