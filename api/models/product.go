package models

import "time"

type Product struct {
	Id            string    `json:"id"`
	Name          string    `json:"name"`
	Price         float64   `json:"price"`
	CategoryID    string    `json:"category_id"`
	BranchPointID string    `json:"branch_point_id"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}
type CreateProduct struct {
	Name          string  `json:"name"`
	Price         float64 `json:"price"`
	CategoryID    string  `json:"category_id"`
	BranchPointID string  `json:"branch_point_id"`
}
type UpdateProduct struct {
	Id            string  `json:"id"`
	Name          string  `json:"name"`
	Price         float64 `json:"price"`
	CategoryID    string  `json:"category_id"`
	BranchPointID string  `json:"branch_point_id"`
}
type ProductResponse struct {
	Products []Product `json:"products"`
	Count    int       `json:"count"`
}
