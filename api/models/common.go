package models

type PrimaryKey struct {
	ID string `json:"id"`
}
type UpdateStatus struct {
	Id     string `json:"id"`
	Status string `json:"status"`
}
type GetlistRequest struct {
	Limit  int    `json:"limit"`
	Page   int    `json:"page"`
	Search string `json:"search"`
}
