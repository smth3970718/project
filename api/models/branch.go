package models

import "time"

type Branch struct {
	Id        string    `json:"id"`
	Name      string    `json:"name"`
	Status    string    `json:"status"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
type CreateBranch struct {
	Name string `json:"name"`
	//Status  string `json:"status"`
}

type UpdateBranch struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type BranchResponse struct {
	Branches []Branch `json:"branches"`
	Count    int      `json:"count"`
}
