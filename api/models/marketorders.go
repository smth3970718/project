package models

import "time"

type MarketOrders struct {
	Id        string    `json:"id"`
	Quantity  int       `json:"quantity"`
	ProductID string    `json:"product_id"`
	Date      time.Time `json:"date"`
	Price     float64   `json:"price"`
	Status    string    `json:"status"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
type CreateMarketOrders struct {
	Quantity  int       `json:"quantity"`
	ProductID string    `json:"product_id"`
	Date      time.Time `json:"date"`
	Price     float64   `json:"price"`
	Status    string    `json:"status"`
}
type UpdateMarketOrders struct {
	Id        string    `json:"id"`
	Quantity  int       `json:"quantity"`
	ProductID string    `json:"product_id"`
	Date      time.Time `json:"date"`
	Price     float64   `json:"price"`
}

type MarketOrdersResponse struct {
	MarketOrders []MarketOrders `json:"marketOrders"`
	Count        int            `json:"count"`
}
