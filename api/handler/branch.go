package handler

import (
	"context"
	"fmt"
	"net/http"
	"project/api/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateBranch godoc
// @Router       /branch [POST]
// @Summary      Creates a new branch
// @Description  create a new branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        branch body models.CreateBranch false "branch"
// @Success      201  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateBranch(c *gin.Context) {

	createbranch := models.CreateBranch{}
	if err := c.ShouldBindJSON(&createbranch); err != nil {
		handleResponse(c, h.log, "error creating branch handler", 400, err.Error())
		return
	}

	response, err := h.services.Branch().Create(context.Background(), createbranch)
	if err != nil {
		handleResponse(c, h.log, "error creating branch handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "branch created successfully", 201, response)
}

// GetBranch godoc
// @Router       /branch/{id} [GET]
// @Summary      Get branch by id
// @Description  get branch by id
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        id path string true "branch_id"
// @Success      201  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranch(c *gin.Context) {
	id := c.Param("id")
	response, err := h.services.Branch().GetById(context.Background(), models.PrimaryKey{ID: id})
	if err != nil {
		handleResponse(c, h.log, "error getting branch handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "", 201, response)

}

// GetBranchList godoc
// @Router       /branchs [GET]
// @Summary      Get branch list
// @Description  get branch list
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param 		 page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.BranchResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranchList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	branches, err := h.services.Branch().GetList(ctx, models.GetlistRequest{
		Page:   page,
		Limit:  limit,
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error is while getting branch list", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, branches)
}

// UpdateBranch godoc
// @Router       /branch/{id} [PUT]
// @Summary      Update branch
// @Description  update branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Param 		 branch body models.UpdateBranch false "branch"
// @Success      200  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateBranch(c *gin.Context) {
	uid := c.Param("id")

	branch := models.UpdateBranch{}
	if err := c.ShouldBindJSON(&branch); err != nil {
		handleResponse(c, h.log, "error is wile reading from body", http.StatusBadRequest, err.Error())
		return
	}

	branch.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	updatedBranch, err := h.services.Branch().Update(ctx, branch)
	if err != nil {
		handleResponse(c, h.log, "error is while updating branch", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, updatedBranch)
}

// DeleteBranch godoc
// @Router       /branch/{id} [DELETE]
// @Summary      Delete branch
// @Description  delete branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteBranch(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	if err := h.services.Branch().Delete(ctx, models.PrimaryKey{ID: uid}); err != nil {
		handleResponse(c, h.log, "error is while delting branch", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, "branch deleted!")
}

// StatusBranch godoc
// @Router       /branchs/{id} [PATCH]
// @Summary      Update branch status
// @Description  update branch status
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Success      200  {object}  models.PrimaryKey
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) StatusBranch(c *gin.Context) {
	id := c.Param("id")
	fmt.Println("id", id)
	fmt.Println("rrrrrrrrrrrrrrrrrrrrrrrrrrr")
	updatedid, err := h.services.Branch().UpdateStatusBranch(context.Background(), models.PrimaryKey{ID: id})
	if err != nil {
		handleResponse(c, h.log, "error is while updating status", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", 200, updatedid)
}
