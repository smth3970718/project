package handler

import (
	"context"
	"net/http"
	"project/api/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateProduct godoc
// @Router       /product [POST]
// @Summary      Creates a new product
// @Description  create a new product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        product body models.CreateProduct false "product"
// @Success      201  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateProduct(c *gin.Context) {
	createproduct := models.CreateProduct{}
	if err := c.ShouldBindJSON(&createproduct); err != nil {
		handleResponse(c, h.log, "error creating product handler", 400, err.Error())
		return
	}
	response, err := h.services.Product().Create(context.Background(), createproduct)
	if err != nil {
		handleResponse(c, h.log, "error creating product handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "product created successfully", 201, response)
}

// GetProduct godoc
// @Router       /product/{id} [GET]
// @Summary      Get product by id
// @Description  get product by id
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        id path string true "product_id"
// @Success      201  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProduct(c *gin.Context) {
	id := c.Param("id")
	response, err := h.services.Product().GetById(context.Background(), models.PrimaryKey{ID: id})
	if err != nil {
		handleResponse(c, h.log, "error getting product handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "success", 201, response)
}

// GetProductList godoc
// @Router       /products [GET]
// @Summary      Get product list
// @Description  get product list
// @Tags         product
// @Accept       json
// @Produce      json
// @Param 		 page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.ProductResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProductList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	products, err := h.services.Product().GetList(ctx, models.GetlistRequest{
		Page:   page,
		Limit:  limit,
		Search: search,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting product list", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, products)
}

// UpdateProduct godoc
// @Router       /product/{id} [PUT]
// @Summary      Update product
// @Description  update product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Param 		 product body models.UpdateProduct false "product"
// @Success      200  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateProduct(c *gin.Context) {
	id := c.Param("id")
	updateproduct := models.UpdateProduct{}
	if err := c.ShouldBindJSON(&updateproduct); err != nil {
		handleResponse(c, h.log, "error updating product handler", 400, err.Error())
		return
	}
	updateproduct.Id = id
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	updatedProduct, err := h.services.Product().Update(ctx, updateproduct)
	if err != nil {
		handleResponse(c, h.log, "error updating product handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, updatedProduct)
}

// DeleteProduct godoc
// @Router       /product/{id} [DELETE]
// @Summary      Delete product
// @Description  delete product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "product_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteProduct(c *gin.Context) {
	id := c.Param("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if err := h.services.Product().Delete(ctx, models.PrimaryKey{ID: id}); err != nil {
		handleResponse(c, h.log, "error deleting product handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, "product deleted!")
}
