package handler

import (
	"context"
	"net/http"
	"project/api/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateCategory godoc
// @Router       /category [POST]
// @Summary      Creates a new category
// @Description  create a new category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        category body models.CreateCategory false "category"
// @Success      201  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCategory(c *gin.Context) {
	createcategory := models.CreateCategory{}
	if err := c.ShouldBindJSON(&createcategory); err != nil {
		handleResponse(c, h.log, "error creating category handler", 400, err.Error())
		return
	}
	response, err := h.services.Category().Create(context.Background(), createcategory)
	if err != nil {
		handleResponse(c, h.log, "error creating category handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "category created successfully", 201, response)
}

// GetCategory godoc
// @Router       /category/{id} [GET]
// @Summary      Get category by id
// @Description  get category by id
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        id path string true "category_id"
// @Success      201  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCategory(c *gin.Context) {
	id := c.Param("id")
	response, err := h.services.Category().GetById(context.Background(), models.PrimaryKey{ID: id})
	if err != nil {
		handleResponse(c, h.log, "error getting category handler", 500, err.Error())
		return

	}
	handleResponse(c, h.log, "success", 201, response)
}

// GetCategoryList godoc
// @Router       /categorys [GET]
// @Summary      Get category list
// @Description  get category list
// @Tags         category
// @Accept       json
// @Produce      json
// @Param 		 page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.CategoryResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCategoryList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	categories, err := h.services.Category().GetList(ctx, models.GetlistRequest{Limit: limit, Search: search, Page: page})
	if err != nil {
		handleResponse(c, h.log, "error is while getting category list", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, categories)

}

// UpdateCategory godoc
// @Router       /category/{id} [PUT]
// @Summary      Update category
// @Description  update category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param 		 id path string true "category_id"
// @Param 		 category body models.UpdateCategory false "category"
// @Success      200  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCategory(c *gin.Context) {
	id := c.Param("id")
	updatecategory := models.UpdateCategory{}
	if err := c.ShouldBindJSON(&updatecategory); err != nil {
		handleResponse(c, h.log, "error updating category handler", 400, err.Error())
		return
	}
	updatecategory.Id = id
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	updatedCategory, err := h.services.Category().Update(ctx, updatecategory)
	if err != nil {
		handleResponse(c, h.log, "error updating category handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, updatedCategory)

}

// DeleteCategory godoc
// @Router       /category/{id} [DELETE]
// @Summary      Delete category
// @Description  delete category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param 		 id path string true "category_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCategory(c *gin.Context) {
	id := c.Param("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if err := h.services.Category().Delete(ctx, models.PrimaryKey{ID: id}); err != nil {
		handleResponse(c, h.log, "error deleting category handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, "category deleted!")
}
