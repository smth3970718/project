package handler

import (
	"context"
	"net/http"
	"project/api/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateBranchPoint godoc
// @Router       /branchpoint [POST]
// @Summary      Creates a new branchpoint
// @Description  create a new branchpoint
// @Tags         branch_point
// @Accept       json
// @Produce      json
// @Param        branch_point body models.CreateBranchPoint false "branch_point"
// @Success      201  {object}  models.BranchPoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateBranchPoint(c *gin.Context) {
	createbranchpoint := models.CreateBranchPoint{}

	if err := c.ShouldBindJSON(&createbranchpoint); err != nil {
		handleResponse(c, h.log, "not reuqired body entered", 400, err.Error())
		return
	}
	response, err := h.services.BranchPoint().Create(context.Background(), createbranchpoint)
	if err != nil {
		handleResponse(c, h.log, "error creating branchpoint", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "branchpoint created successfully", 201, response)
}

// GetBranchPoint godoc
// @Router       /branchpoint/{id} [GET]
// @Summary      Get branch by id
// @Description  get branch by id
// @Tags         branch_point
// @Accept       json
// @Produce      json
// @Param        id path string true "branch_point_id"
// @Success      201  {object}  models.BranchPoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranchPoint(c *gin.Context) {
	id := c.Param("id")
	response, err := h.services.BranchPoint().GetById(context.Background(), models.PrimaryKey{ID: id})
	if err != nil {
		handleResponse(c, h.log, "error getting branchpoint handler", 500, err.Error())
		return

	}
	handleResponse(c, h.log, "success", 200, response)

}

// GetBranchPointList godoc
// @Router       /branchpoints [GET]
// @Summary      Get branchpoint list
// @Description  get branchpoint list
// @Tags         branch_point
// @Accept       json
// @Produce      json
// @Param 		 page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.BranchPointResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranchPointList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	branchpoints, err := h.services.BranchPoint().GetList(context.Background(), models.GetlistRequest{Limit: limit, Page: page, Search: search})
	if err != nil {
		handleResponse(c, h.log, "error is while getting branchpoint list", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, branchpoints)

}

// UpdateBranchPoint godoc
// @Router       /branchpoint/{id} [PUT]
// @Summary      Update branch_point
// @Description  update branch_point
// @Tags         branch_point
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Param 		 branch_point body models.UpdateBranchPoint false "branch_point"
// @Success      200  {object}  models.BranchPoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateBranchPoint(c *gin.Context) {

	id := c.Param("id")
	branch_point := models.UpdateBranchPoint{}
	if err := c.ShouldBindJSON(&branch_point); err != nil {
		handleResponse(c, h.log, "not required body entered", 400, err.Error())
		return
	}
	branch_point.ID = id
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	updatedBranchPoint, err := h.services.BranchPoint().Update(ctx, branch_point)
	if err != nil {
		handleResponse(c, h.log, "error is while updating branchpoint", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, updatedBranchPoint)
}

// DeleteBranchPoint godoc
// @Router       /branchpoint/{id} [DELETE]
// @Summary      Delete branch_point
// @Description  delete branch_point
// @Tags         branch_point
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_point_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteBranchPoint(c *gin.Context) {
	id := c.Param("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if err := h.services.BranchPoint().Delete(ctx, models.PrimaryKey{ID: id}); err != nil {
		handleResponse(c, h.log, "error is while delting branchpoint", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, "branch deleted!")
}

// UpdateStatusBranchPoint godoc
// @Router       /branchpointstatus/{id} [PUT]
// @Summary      Update branch_point status
// @Description  update branch_point status
// @Tags         branch_point
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Success      200  {object}  models.PrimaryKey
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateStatusBranchPoint(c *gin.Context) {
	id := c.Param("id")
	updatedid, err := h.services.BranchPoint().UpdateStatusBranchPoint(context.Background(), models.UpdateStatus{Id: id})
	if err != nil {
		handleResponse(c, h.log, "error is while updating branchpoint", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, updatedid)
}
