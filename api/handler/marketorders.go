package handler

import (
	"context"
	"fmt"
	"net/http"
	"project/api/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateMarketOrders godoc
// @Router       /market_order [POST]
// @Summary      Creates a new market_order
// @Description  create a new market_order
// @Tags         market_order
// @Accept       json
// @Produce      json
// @Param        market body models.CreateMarketOrders false "market"
// @Success      201  {object}  models.MarketOrders
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateMarketOrders(c *gin.Context) {
	createmarketorder := models.CreateMarketOrders{}
	if err := c.ShouldBindJSON(&createmarketorder); err != nil {
		handleResponse(c, h.log, "error creating market_order handler", 400, err.Error())
		return
	}
	response, err := h.services.MarketOrders().Create(context.Background(), createmarketorder)
	if err != nil {
		handleResponse(c, h.log, "error creating market_order handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "market_order created successfully", 201, response)
}

// GetMarketOrders godoc
// @Router       /market_order/{id} [GET]
// @Summary      Get market by id
// @Description  get market by id
// @Tags         market_order
// @Accept       json
// @Produce      json
// @Param        id path string true "market_order_id"
// @Success      201  {object}  models.MarketOrders
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetMarketOrders(c *gin.Context) {
	id := c.Param("id")
	fmt.Println("555555555555",id)
	response, err := h.services.MarketOrders().GetById(context.Background(), models.PrimaryKey{ID: id})
	if err != nil {
		handleResponse(c, h.log, "error getting market_order handler", 500, err.Error())
		fmt.Println(err.Error())
		return
	}
	handleResponse(c, h.log, "success", 201, response)
}

// GetMarketOrdersList godoc
// @Router       /market_order [GET]
// @Summary      Get market_order list
// @Description  get market_order list
// @Tags         market_orders
// @Accept       json
// @Produce      json
// @Param 		 page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.MarketOrdersResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetMarketOrdersList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	markets, err := h.services.MarketOrders().GetList(ctx, models.GetlistRequest{Limit: limit, Search: search, Page: page})
	if err != nil {
		handleResponse(c, h.log, "error is while getting market_order list", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, markets)
}

// UpdateMarketOrders godoc
// @Router       /market_order/{id} [PUT]
// @Summary      Update market
// @Description  update market
// @Tags         market_order
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Param 		 market_order body models.UpdateMarketOrders false "market_orders"
// @Success      200  {object}  models.MarketOrders
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateMarketOrders(c *gin.Context) {
	id := c.Param("id")
	updatemarketorder := models.UpdateMarketOrders{}
	if err := c.ShouldBindJSON(&updatemarketorder); err != nil {
		handleResponse(c, h.log, "error updating market_order handler", 400, err.Error())
		return
	}
	updatemarketorder.Id = id
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	updatedMarket, err := h.services.MarketOrders().Update(ctx, updatemarketorder)
	if err != nil {
		handleResponse(c, h.log, "error updating market_order handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, updatedMarket)
}

// DeleteMarketOrders godoc
// @Router       /market_order/{id} [DELETE]
// @Summary      Delete market_order
// @Description  delete market_order
// @Tags         market_order
// @Accept       json
// @Produce      json
// @Param 		 id path string true "market_order"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteMarketOrders(c *gin.Context) {
	id := c.Param("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	if err := h.services.MarketOrders().Delete(ctx, models.PrimaryKey{ID: id}); err != nil {
		handleResponse(c, h.log, "error is while delting market_order", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "market_order deleted!")
}

// ListSavingOrders godoc
// @Router          /market_order  [GET]
// @Summary         delete market_order
// @Description     delete market_order
// @Tags            market_order
// @Accept          json
// @Produce         json
// @Param 		    page query string false "page"
// @Param 		    limit query string false "limit"
// @Param 		    search query string false "search"
// @Success         200  {object}  models.OrdersHistoryResponse
// @Failure         400  {object}  models.Response
// @Failure         404  {object}  models.Response
// @Failure         500  {object}  models.Response
func (h Handler) ListSavingOrders(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	repsonse, err := h.services.MarketOrders().ListSavingOrders(ctx, models.GetlistRequestOrder{Limit: limit, Page: page, Search: search})

	if err != nil {
		handleResponse(c, h.log, "error is while getting market_order list", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, repsonse)
}

// UpdateMarketOrdersStatus godoc
// @Router       /market_order_status/{id} [PUT]
// @Summary      Update market
// @Description  update market
// @Tags         market_order
// @Accept       json
// @Produce      json
// @Param 		 market_order_status body models.UpdateStatus false "status"
// @Success      200  {object}  models.UpdateStatus
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateMarketOrdersStatus(c *gin.Context) {
	status := models.UpdateStatus{}
	if err := c.ShouldBindJSON(&status); err != nil {
		handleResponse(c, h.log, "bad request for updating status of market orders", 400, err)
		return
	}
	
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	updatedStatus, err := h.services.MarketOrders().UpdateStatus(ctx, status)
	if err != nil {
		handleResponse(c, h.log, "error updating market_order handler", 500, err.Error())
		return
	}
	handleResponse(c, h.log, "", http.StatusOK, models.UpdateStatus{Id: updatedStatus, Status: status.Status})

}
