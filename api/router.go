package api

import (
	_ "project/api/docs"
	"project/api/handler"
	"project/pkg/logger"
	"project/service"

	"github.com/gin-gonic/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// New ...
// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(services service.IServiceManager, log logger.ILogger) *gin.Engine {
	h := handler.New(services, log)

	r := gin.New()
	r.Use(gin.Logger())
	// r.Use(traceRequest)
	{
		// r.POST("user")
		// r.PUT("user")

		r.POST("/branch", h.CreateBranch)
		r.GET("/branch/:id", h.GetBranch)
		r.GET("/branchs", h.GetBranchList)
		r.PUT("/branch/:id", h.UpdateBranch)
		r.DELETE("/branch/:id", h.DeleteBranch)
		r.PATCH("/branchs/:id", h.StatusBranch)

		r.POST("/branchpoint", h.CreateBranchPoint)
		r.GET("/branchpoint/:id", h.GetBranchPoint)
		r.GET("/branchpoints", h.GetBranchPointList)
		r.PUT("/branchpoint/:id", h.UpdateBranchPoint)
		r.DELETE("/branchpoint/:id", h.DeleteBranchPoint)
		r.PUT("/branchpointstatus/:id", h.UpdateStatusBranchPoint)

		r.POST("/category", h.CreateCategory)
		r.GET("/category/:id", h.GetCategory)
		r.GET("/categorys", h.GetCategoryList)
		r.PUT("/category/:id", h.UpdateCategory)
		r.DELETE("/category/:id", h.DeleteCategory)

		r.POST("/product", h.CreateProduct)
		r.GET("/product/:id", h.GetProduct)
		r.GET("/products", h.GetProductList)
		r.PUT("/product/:id", h.UpdateProduct)
		r.DELETE("/product/:id", h.DeleteProduct)

		r.POST("/market_order", h.CreateMarketOrders)
		r.GET("/market_order/:id", h.GetMarketOrders)
		r.GET("/market_orders", h.GetMarketOrdersList)
		r.PUT("/market_order/:id", h.UpdateMarketOrders)
		r.DELETE("/market_order/:id", h.DeleteMarketOrders)
		r.PUT("/market_order_status/:id", h.UpdateMarketOrdersStatus)

		r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}
	return r

}

// func authenticateMiddleware(c *gin.Context) {
// 	auth := c.GetHeader("Authorization")
// 	if auth == "" {
// 		c.AbortWithError(http.StatusUnauthorized, errors.New("unauthorized"))
// 	} else {
// 		c.Next()
// 	}

// }

// func traceRequest(c *gin.Context) {
// 	beforeRequest(c)

// 	c.Next()

// 	afterRequest(c)
// }

// func beforeRequest(c *gin.Context) {
// 	startTime := time.Now()

// 	c.Set("start_time", startTime)

// 	log.Println("start time:", startTime.Format("2006-01-02 15:04:05.0000"), "path:", c.Request.URL.Path)
// }

// func afterRequest(c *gin.Context) {
// 	startTime, exists := c.Get("start_time")
// 	if !exists {
// 		startTime = time.Now()
// 	}

// 	duration := time.Since(startTime.(time.Time)).Seconds()

// 	log.Println("end time:", time.Now().Format("2006-01-02 15:04:05.0000"), "duration:", duration, "method:", c.Request.Method)
// 	fmt.Println()
// }
