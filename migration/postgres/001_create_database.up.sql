
CREATE TYPE status_enum AS ENUM(
    'not_paid',
    'paid',
    'not_fully_paid'
);

CREATE TYPE branch_enum AS ENUM(
    'not_paid',
    'fully_paid' 
);


CREATE TABLE IF NOT EXISTS users (
    id UUID PRIMARY KEY,
    FullName VARCHAR(100),
    phone VARCHAR(13),
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITHOUT TIME ZONE,
    deleted_at INT DEFAULT 0
);

-- Create table "branch" if it doesn't exist
CREATE TABLE IF NOT EXISTS branch (
    id UUID PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    status branch_enum DEFAULT 'not_paid',
    date DATE DEFAULT current_date,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITHOUT TIME ZONE,
    deleted_at INT DEFAULT 0
);

-- Create table "branchpoint" if it doesn't exist
CREATE TABLE IF NOT EXISTS branchpoint (
    id UUID PRIMARY KEY,
    name VARCHAR(100),
    address VARCHAR(50),
    branch_id UUID REFERENCES branch(id),
    status status_enum DEFAULT 'not_paid',
    date DATE DEFAULT current_date,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITHOUT TIME ZONE,
    deleted_at INT DEFAULT 0
);

-- Create table "category" if it doesn't exist
CREATE TABLE IF NOT EXISTS category (
    id UUID PRIMARY KEY,
    name VARCHAR(100),
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITHOUT TIME ZONE,
    deleted_at INT DEFAULT 0
);

-- Create table "product" if it doesn't exist
CREATE TABLE IF NOT EXISTS product (
    id UUID PRIMARY KEY,
    name VARCHAR(100),
    price NUMERIC(10, 2),
    category_id UUID REFERENCES category(id),
    branch_point_id UUID REFERENCES branchpoint(id),
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITHOUT TIME ZONE,
    deleted_at INT DEFAULT 0
);

-- Create table "marketOrders" if it doesn't exist
CREATE TABLE IF NOT EXISTS marketOrders (
    id UUID PRIMARY KEY,
    product_id UUID REFERENCES product(id),
    quantity INT,
    price NUMERIC(10, 2),
    date DATE DEFAULT current_date,
    status status_enum DEFAULT 'not_paid',
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITHOUT TIME ZONE,
    deleted_at INT DEFAULT 0
);

-- Create table "ordersHistory" if it doesn't exist
CREATE TABLE IF NOT EXISTS ordersHistory (
    id UUID PRIMARY KEY,
    marketorders_id UUID REFERENCES marketOrders(id),
    total_sum FLOAT NOT NULL,
    created_at DATE,
    deleted_at INT DEFAULT 0
);
